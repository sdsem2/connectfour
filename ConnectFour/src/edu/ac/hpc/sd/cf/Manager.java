package edu.ac.hpc.sd.cf;

import java.io.IOException;

import edu.ac.hpc.sd.cf.impl.CFException;

/**
 * Stars and manages the flow of the game until completion.
 * @author s1260124
 *
 */
public interface Manager {
	void startGame() throws IOException,CFException;
}
