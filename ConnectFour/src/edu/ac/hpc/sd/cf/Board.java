package edu.ac.hpc.sd.cf;

/**
 * Connect Four board on which the 
 * game is played. Contains the details and status
 * of the player and board.
 * Actions on the board will be carried using this interface.
 * given player.
 * @author s1260124
 *
 */
public interface Board {	
	void makeMove(int column);
	void undoMove();
	boolean hasValidMovesLeft();
	int winnerIs();
	int getCurrentPlayer();
	boolean validMove(int column);
	int getStrength();
}
