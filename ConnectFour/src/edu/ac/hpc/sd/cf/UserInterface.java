package edu.ac.hpc.sd.cf;

import edu.ac.hpc.sd.cf.impl.CFException;

/**
 * Retrieves the user inputs for getting the slots
 * to be filled.
 * @author s1260124
 *
 */
public interface UserInterface {
	int getSlotFromUser() throws CFException;
}
