package edu.ac.hpc.sd.cf.impl;

import edu.ac.hpc.sd.cf.AI;
import edu.ac.hpc.sd.cf.Board;

/**
 * 
 * @author s1260124
 * 
 */
public class AIImpl implements AI {

	/**
	 * Compute the best possible move corresponding to current state of the
	 * board
	 */
	public int getMove(Board board) throws CFException {
		if (null != board) {
			int[] moves = new int[7];
			int highest = 0;
			System.out.print(Constants.SYSTEMCOMPUTING);
			float percentage = 0;
			for (int i = 0; i < 7; i++) {
				moves[i] = Integer.MIN_VALUE;
				if (board.validMove(i)) {
					board.makeMove(i);
					moves[i] = minValue(board, Constants.PLYVALUE);
					if (moves[i] >= moves[highest])
						highest = i;
					board.undoMove();
				}
				percentage++;
				System.out.print(Math.floor(((percentage / 7) * 100) / 0.5)
						* 0.5 + "%\t");// To display the progress in the console
			}
			return highest;
		} else {
			throw new CFException(Constants.SYSTEMCOMPUTINGEXCEPTION);
		}
	}

	/**
	 * Identifying the minvalue in MiniMax algorithm. Computes the move with
	 * least possible winning value for the opponent.
	 * 
	 * @param cBoard
	 * @param ply
	 * @return
	 * @throws CFException
	 */
	// don't change this unless you understand it
	private int minValue(Board cBoard, int ply) throws CFException {
		int[] moves = new int[7];
		int lowest = 0;
		for (int i = 0; i < 7; i++) {
			moves[i] = Integer.MAX_VALUE;
			if (cBoard.validMove(i)) {
				cBoard.makeMove(i);
				if ((cBoard.winnerIs() == 0) && ply > 0) {
					moves[i] = maxValue(cBoard, ply - 1);
				} else {
					moves[i] = -cBoard.getStrength();
				}
				if (moves[i] < moves[lowest])
					lowest = i;
				cBoard.undoMove();
			}
		}
		return moves[lowest];
	}

	/**
	 * Identifying the maxvalue in MiniMax algorithm. Computes the move with
	 * maximum possible winning value for the current player.
	 * 
	 * @param cBoard
	 * @param ply
	 * @return
	 * @throws CFException
	 */
	// careful with this
	private int maxValue(Board cBoard, int ply) throws CFException {
		int[] moves = new int[7];
		int highest = 0;
		for (int i = 0; i < 7; i++) {
			moves[i] = Integer.MAX_VALUE;
			if (cBoard.validMove(i)) {
				cBoard.makeMove(i);
				if ((cBoard.winnerIs() == 0) && ply > 0) {
					moves[i] = minValue(cBoard, ply - 1);
				} else{
					moves[i] = -cBoard.getStrength();
				}
				if (moves[i] < moves[highest])
					highest = i;
				cBoard.undoMove();
			}
		}
		return moves[highest];

	}
}
