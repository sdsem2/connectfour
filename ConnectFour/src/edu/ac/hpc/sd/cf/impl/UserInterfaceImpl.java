package edu.ac.hpc.sd.cf.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import edu.ac.hpc.sd.cf.UserInterface;

/**
 * Contains the UI implementation for the game. Currently supports command line
 * interface. Should be modified to support other interfaces.
 *
 * @author s1260124
 *
 */
public class UserInterfaceImpl implements UserInterface {
    
	BufferedReader userInput;
    
	/**
	 * Command line interface for getting input from user.
	 */
	public UserInterfaceImpl() {
		userInput = new BufferedReader(new InputStreamReader(System.in));
	}
    
	@Override
	/**
	 * Retrieves the slot number(Column Id) from the user
	 */
	public int getSlotFromUser() throws CFException {
		System.out.println(Constants.USERINPUTREQUESTMESSAGE);
		try {
			return validateUserInput(Integer.parseInt(userInput.readLine()));
		} catch (IOException ioException) {
			throw new CFException(Constants.IOEXCEPTIONMESSAGE);
		} catch (NumberFormatException numberFormatException) {
			throw new CFException(Constants.INVALIDINPUT);
		}
	}
    
	/**
	 * Validates slot number for the user and returns the slot number. Throws
	 * CFException for invalid input.
	 *
	 * @param slotNumber
	 * @return slotNumber
	 * @throws CFException
	 */
	public int validateUserInput(int slotNumber) throws CFException {
		if (slotNumber < 0 || slotNumber > 6) {
			throw new CFException(Constants.SLOTNUMBERNOTINRANGE);
		}
		return slotNumber;
	}
    
}
