package edu.ac.hpc.sd.cf.impl;

import java.io.IOException;

import edu.ac.hpc.sd.cf.AI;
import edu.ac.hpc.sd.cf.Board;
import edu.ac.hpc.sd.cf.Manager;
import edu.ac.hpc.sd.cf.UserInterface;

public class ManagerImpl implements Manager {

	/**
	 * Main method to start the execution with user inputs from command line
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String args[]) {
		try{
			new ManagerImpl().startGame();
		}
		catch(CFException cfException){
			System.out.println(cfException.getMessage());
		}
	}
	
	@Override
	/**
	 * Manages the complete simulation.
	 * Exits when the game is complete.
	 */
	public void startGame() throws CFException {

		UserInterface ui = new UserInterfaceImpl();
		Board board = new BoardImpl(Constants.BOARDCOLUMNS, Constants.BOARDROWS);
		AI ai=new AIImpl();
		boolean uiError=false;
		while ((board.winnerIs() == 0) && board.hasValidMovesLeft()) {
			if (board.getCurrentPlayer() == Constants.PLAYER_ONE) {
				try{
					board.makeMove(ui.getSlotFromUser());//Get the slot id from user[Column Value]
				}
				catch(CFException cfException){
					System.out.println(cfException.getMessage());
					uiError=true;
				}
			} 
			else{
				board.makeMove(ai.getMove(board));// Get the slot id computed by the system[Column Value]
			}
			if(!uiError){
				System.out.println("\nConnect Four Board Current State[Player One (O)/System (X)]\n");
				System.out.println(board);
				System.out.println("***********************************************************");
			}
		}
		/* Game Complete */
		if (board.winnerIs() == Constants.PLAYER_ONE)
			System.out.println(Constants.PLAYERONEWINMESSAGE);
		else
			System.out.println(Constants.PLAYERTWOWINMESSAGE);
	}

}
