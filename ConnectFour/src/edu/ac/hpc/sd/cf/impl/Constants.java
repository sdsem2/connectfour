package edu.ac.hpc.sd.cf.impl;

/**
 * Contains the constants related to the connect four game.
 * @author s1260124
 *
 */
public class Constants {
    
	//Player Details
	public final static int PLAYER_ONE = 1;
	public final static int PLAYERONE_STRENGTH = 16;
	public final static int PLAYER_TWO = -1;
	public final static int PLAYERTWO_STRENGTH = -16;
	public final static int EMPTY = 0;
	//Board Details
	public final static int BOARDCOLUMNS=7;
	public final static int BOARDROWS=6;
	public final static int WINNINGPOSSIBILITY=69;
	public final static int WINNINGPATTERN=4;//Horizontal,Vertical,Forward Diagonal and Backward Diagonal
	//Winning Message
	public final static String PLAYERONEWINMESSAGE="Game Over, Player 1 has won the game";
	public final static String PLAYERTWOWINMESSAGE="Game Over, Player 2 has won the game";
	//UI Messages
	public final static String USERINPUTREQUESTMESSAGE="Please enter the slot number(0-6)\n";
	public final static String SLOTNUMBERNOTINRANGE="Slot Entered is wrong!! Please enter the number between [0-6]\n";
	public final static String INVALIDINPUT="Invalid Input, Please enter a valid number";
	public final static String IOEXCEPTIONMESSAGE="Exception in processing the intput:";
	//AI Values
	public final static int PLYVALUE=6;
	//System progress
	public final static String SYSTEMCOMPUTING="--\tSystem Computing the Move\t--";
	public final static String SYSTEMCOMPUTINGEXCEPTION="System unable to compute the move";
	
	
}
