package edu.ac.hpc.sd.cf.impl;

public class CFException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7440498351700760533L;
	public CFException(){
		
	}
	public CFException(String message){
		super(message);
	}

}
