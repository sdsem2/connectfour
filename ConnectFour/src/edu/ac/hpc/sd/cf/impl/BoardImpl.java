package edu.ac.hpc.sd.cf.impl;

import edu.ac.hpc.sd.cf.Board;

/**
 *
 * @author s1260124
 *
 */
public class BoardImpl implements Board {
    
	// class level data members
	Point[][] grid;
	private int[] heights;
    
	private int cols;
	private int rows;
    
	private int[] movesList;
	private int leftMoves;
    
	private int currentPlayer;
	Point[][] claimEven;
    
	/**
	 * Initialising the connect four board for the given number of rows and
	 * columns Derivatives like grid, heights moves list and left moves will be
	 * intialised.
	 *
	 * @param columns
	 * @param inrows
	 */
	public BoardImpl(int columns, int inrows) {
		cols = columns;
		rows = inrows;
		grid = new Point[cols][rows];
		heights = new int[cols];
		movesList = new int[cols * rows];
		leftMoves = -1;
		for (int x = 0; x < cols; x++) {
			heights[x] = 0;
			for (int y = 0; y < rows; y++)
				grid[x][y] = new Point(x, y, Constants.EMPTY);
            
		}
		generateCL();
		currentPlayer = Constants.PLAYER_ONE;
	}
    
	// Claim Even Board Genarator
	/**
	 * Generates the possibility of winning combination using CliamEven Strategy
	 */
	private void generateCL() {
		claimEven = new Point[Constants.WINNINGPOSSIBILITY][Constants.WINNINGPATTERN];
		int count = 0;
		// Horizontal Segment Generator
		for (int y = 0; y < rows; y++) {
			for (int x = 0; x < cols - 3; x++) {
				Point[] temp = new Point[Constants.WINNINGPATTERN];
				for (int i = x; i < x + 4; i++)
					temp[i - x] = grid[i][y];
				claimEven[count] = temp;
				count++;
			}
            
		}
		// Vertical Segment Generator
		for (int x = 0; x < cols; x++) {
			for (int y = 0; y < rows - 3; y++) {
				Point[] temp = new Point[Constants.WINNINGPATTERN];
				for (int i = y; i < y + 4; i++)
					temp[i - y] = grid[x][i];
				claimEven[count] = temp;
				count++;
			}
            
		}
		// Diagonal Segment Generator - Forward
		for (int x = 0; x < cols - 3; x++) {
			for (int y = 0; y < rows - 3; y++) {
				Point[] temp = new Point[Constants.WINNINGPATTERN];
				for (int t = x, i = y; t < x + 4 && i < y + 4; t++, i++)
					temp[i - y] = grid[t][i];
				claimEven[count] = temp;
				count++;
			}
            
		}
		// Diagonal Segment Generator - Backward
		for (int x = 0; x < cols - 3; x++) {
			for (int y = rows - 1; y > rows - 4; y--) {
				Point[] temp = new Point[Constants.WINNINGPATTERN];
				for (int t = x, i = y; t < x + 4 && i > -1; t++, i--)
					temp[t - x] = grid[t][i];
				claimEven[count] = temp;
				count++;
			}
		}
	}
    
	@Override
	/**
	 * Fill the slot for the column provided.
	 * Moves list will be incremented with the
	 * value of the column provided.
	 * Control will be provided to the other player
	 * after making the move.
	 * @param coloumn
	 */
	public void makeMove(int column) {
		grid[column][heights[column]].setState(currentPlayer);
		heights[column]++;
		leftMoves++;
		movesList[leftMoves] = column;
		currentPlayer = -currentPlayer;
	}
    
	@Override
	/**
	 * Internal method to undo the created move,
	 * Used for manipulating the values when
	 * system computes for deriving
	 * the next best slot.
	 */
	public void undoMove() {
		grid[movesList[leftMoves]][heights[movesList[leftMoves]] - 1]
        .setState(Constants.EMPTY);
		heights[movesList[leftMoves]]--;
		leftMoves--;
		currentPlayer = -currentPlayer;
	}
    
	@Override
	/**
	 * Returns true if the game can be continued,
	 * false for the end of game.
	 */
	public boolean hasValidMovesLeft() {
		return leftMoves < movesList.length - 1;
	}
    
	@Override
	/**
	 * Returns the winner of the game.
	 * Player's are represented in Integer.
	 */
	public int winnerIs() {
		for (int i = 0; i < claimEven.length; i++)
			if (getScore(claimEven[i]) == 4) {
				return Constants.PLAYER_ONE;
			} else if (getScore(claimEven[i]) == -4)
				return Constants.PLAYER_TWO;
		return 0;
	}
    
	@Override
	/**
	 * Returns the current player
	 * making the required move.
	 */
	public int getCurrentPlayer() {
		return currentPlayer;
	}
    
	@Override
	/**
	 * Returns true if the move
	 * is valid and false for invalid move.
	 * @param column
	 */
	public boolean validMove(int column) {
		return heights[column] < rows;
	}
    
	@Override
	/**
	 * Provides the probability of winning strength
	 * of current player after each move
	 */
	public int getStrength() {
		int sum = 0;
		int[] weights = { 0, 1, 10, 50, 600 };
		for (int i = 0; i < claimEven.length; i++) {
			sum += (getScore(claimEven[i]) > 0) ? weights[Math
                                                          .abs(getScore(claimEven[i]))] : -weights[Math
                                                                                                   .abs(getScore(claimEven[i]))];
		}
		return sum
        + (currentPlayer == Constants.PLAYER_ONE ? Constants.PLAYERONE_STRENGTH
           : Constants.PLAYERTWO_STRENGTH);
	}
    
	/**
	 * Computes the score for the given points. Will be calculated for each move
	 * computation.
	 *
	 * @param points
	 * @return
	 */
	private int getScore(Point[] points) {
		int playerone = 0;
		int playertwo = 0;
		for (int i = 0; i < points.length; i++)
			if (points[i].getState() == Constants.PLAYER_ONE)
				playerone++;
			else if (points[i].getState() == Constants.PLAYER_TWO)
				playertwo++;
		if (playerone > 0 && playertwo == 0) {
			return playerone;
		} else if (playertwo > 0 && playerone == 0) {
			return -playertwo;
		}
		return 0;
	}
    
	/**
	 * Converts the board object to String. Formats the board object to visual
	 * connect four for displaying the status of the game in output.
	 */
	public String toString() {
		String temp = "";
		for (int x = 0; x < cols; x++) {
			temp = temp + x;
		}
		temp += "\n";
		for (int x = 0; x < cols; x++) {
			temp = temp + "=";
		}
		temp += "\n";
		for (int y = rows - 1; y > -1; y--) {
			for (int x = 0; x < cols; x++)
				if (grid[x][y].getState() == Constants.EMPTY)
					temp = temp + "-";
				else if (grid[x][y].getState() == Constants.PLAYER_ONE)
					temp = temp + "O";
				else
					temp = temp + "X";
			temp += "\n";
		}
		return temp;
	}
    
}

/**
 * Internal class to hold the points and the state.
 *
 * @author s1260124
 *
 */
class Point {
	int x;
	int y;
	int state;
    
	Point(int xt, int yt, int statet) {
		x = xt;
		y = yt;
		state = statet;
	}
    
	boolean equalsPosition(Point q) {
		return x == q.x && y == q.y;
	}
    
	void setState(int player) {
		state = player;
	}
    
	int getState() {
		return state;
	}
}
