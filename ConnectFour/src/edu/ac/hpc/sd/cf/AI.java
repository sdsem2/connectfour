package edu.ac.hpc.sd.cf;

import edu.ac.hpc.sd.cf.impl.CFException;

/**
 * Computes the best possible for the 
 * given player.
 * @author s1260124
 *
 */
public interface AI {
	/**
	 * Returns the best possible move for the
	 * given status of the board.
	 * @param board
	 * @return slotNumber
	 * @throws CFException
	 */
	public int getMove(Board board) throws CFException;
}
