package edu.ac.hpc.sd.cf.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.ac.hpc.sd.cf.Board;
import edu.ac.hpc.sd.cf.impl.BoardImpl;
/**
 *
 * @author s1260124
 *
 */
public class TestBoardImpl {
    
	private Board board;
	
	@Before
	public void createBoardObject(){
		board=new BoardImpl(7, 6);
	}
	
	@Test
	public void testHasValidMovesLeft() {
		if(!board.hasValidMovesLeft()){
			fail("Valid moves returned false, No Moves have been made yet!!");
		}
	}
	
	@Test
	public void testWinnerIs() {
		if(board.winnerIs()!=0){
			fail("Winner is determined before the game is completed!!");
		}
	}
	
	@Test
	public void testGetStrength(){
		if(board.getStrength()!=16){
			fail("Invalid Strength value for Player ONE, The value should be '16'!!");
		}
	}
	
	@Test
	public void testMakeMove(){
		board.makeMove(0);
		if(board.getStrength()!=-13){
			fail("Invalid Strength value after the move for Slot 'Zero', The value should be '-13'!!");
		}
		if(board.getCurrentPlayer()!=-1){
			fail("Player not changed after the current move!!");
		}
		board.makeMove(0);
		board.makeMove(0);
	}
}
