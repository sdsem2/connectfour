package edu.ac.hpc.sd.cf.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import edu.ac.hpc.sd.cf.AI;
import edu.ac.hpc.sd.cf.Board;
import edu.ac.hpc.sd.cf.impl.AIImpl;
import edu.ac.hpc.sd.cf.impl.BoardImpl;
import edu.ac.hpc.sd.cf.impl.CFException;
/**
 *
 * @author s1260124
 *
 */
public class TestAIImpl {
    
	private Board board;
	private AI ai;
	
	@Before
	public void createBoardObject(){
		board=new BoardImpl(7, 6);
		ai=new AIImpl();
	}
	
	@Test
	public void testGetMove() {
		try{
			if(ai.getMove(board)!=6){
				fail("Invaid move computed by the system, Value should be equal to 6!!");
			}
		}
		catch(CFException cfException){
			fail("Move not successful, returned cfexception");
		}
	}
    
}
