package edu.ac.hpc.sd.cf.impl;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.ac.hpc.sd.cf.UserInterface;
import edu.ac.hpc.sd.cf.impl.CFException;
import edu.ac.hpc.sd.cf.impl.Constants;
import edu.ac.hpc.sd.cf.impl.UserInterfaceImpl;

public class TestUserInterfaceImpl {
    
	@Test
	public void testValidUserInput() {
		UserInterfaceImpl userInterfaceImpl = new UserInterfaceImpl();
		int returnValue = 0;
		try {
			userInterfaceImpl.validateUserInput(1);
		} catch (CFException cfException) {
			fail("Invalid error thrown for valid Slot Number");
		}
	}
    
	@Test
	public void testForInvalidUserInput() {
		UserInterfaceImpl userInterfaceImpl = new UserInterfaceImpl();
		int returnValue = 0;
		try {
			userInterfaceImpl.validateUserInput(-1);
		} catch (CFException cfException) {
			assertTrue("Exception thrown for invalid slot number", cfException
                       .getMessage().equals(Constants.SLOTNUMBERNOTINRANGE));
		}
	}
    
	@Test
	public void testForEmptyUserInput() {
		UserInterfaceImpl userInterfaceImpl = new UserInterfaceImpl();
		int returnValue = 0;
		try {
			userInterfaceImpl.validateUserInput(12324234);
		} catch (CFException cfException) {
			assertTrue("Exception thrown for invalid slot number", cfException
                       .getMessage().equals(Constants.SLOTNUMBERNOTINRANGE));
		}
	}
    
}
